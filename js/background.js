
var details = chrome.app.getDetails();

var settings = new Store("settings", {
    "selectorFollow":    "button.follow-text:visible",
    "delay":             500,
    "selectorFollowing": "button.following-text:visible",
    "selectorUnfollow":  ".unfollow-text",
    "delayUnfollow":     500,
    "selectorSidebar":   ".WhoToFollow, .wtf-module",
    "buttonsTop": 50
});

chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
    switch (request.action) {
        case 'getOptions':  _gaq.push(['_trackEvent', 'twitter.com', 'visited']);
			    			sendResponse(settings.toObject());
                            break;
        case 'clickButton': _gaq.push(['_trackEvent', request.button, 'clicked']);
			    			break;
        case 'follow': 	    _gaq.push(['_trackEvent', 'user', 'followed']);
			    			break;
        case 'unfollow':    _gaq.push(['_trackEvent', 'user', 'unfollowed']);
			    			break;
        case 'dragButtons': 	settings.set('buttonsTop', request.position.top);
        												settings.set('buttonsLeft', request.position.left);
        												break;
        default:            // Unknown action, do nothing
                            break;
    }
  }
);

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-39088567-13']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = 'https://ssl.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
