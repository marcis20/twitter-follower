chrome.runtime.sendMessage({action: 'getOptions'}, function(theOptions) {
    var stop;
    function scrollTo($target, delay, callback) {
    	$('html, body').animate({
    	    scrollTop: $target.offset().top
    	}, delay);
    	$('#twitter-follower-buttons').animate({
    	    top: $target.offset().top
    	}, delay, callback);
    }
    function followOne() {
        var $followButton;
        if (theOptions.sidebar) {
            $followButton = $(theOptions.selectorFollow).first();
        } else {
            $followButton = $(theOptions.selectorFollow).not($(theOptions.selectorSidebar).find(theOptions.selectorFollow)).first();
        }
        if (!stop && $followButton.length) {
        	scrollTo($followButton, parseInt(theOptions.delay), followOne);
        	$followButton.click();
           	chrome.runtime.sendMessage({action: 'follow'});
        } else {
            stopFollowing();
        }
    }
    function unfollowOne() {
        var $followingButton = $(theOptions.selectorFollowing).first();
        if (!stop && $followingButton.length) {
            var $unfollowButton = $followingButton.parent().find(theOptions.selectorUnfollow).first();
            scrollTo($followingButton, parseInt(theOptions.delayUnfollow), unfollowOne);
            $unfollowButton.click();
            chrome.runtime.sendMessage({action: 'unfollow'});
        } else {
            stopUnfollowing();
        }
    }
    function followAll() {
    	trackButton('follow-them-all');
        stop = false;
        $('#follow-them-all').html('STOP').unbind('click').click(stopFollowing);
        $('#unfollow-all').attr('disabled', true);
        followOne();
    }
    function unfollowAll() {
    	trackButton('unfollow-all');
        stop = false;
        $('#unfollow-all').html('STOP').unbind('click').click(stopUnfollowing);
        $('#follow-them-all').attr('disabled', true);
        unfollowOne();        
    }
    function stopFollowing() {
        stop = true;
        $('#follow-them-all').html('Follow them all!').unbind('click').click(followAll);
        $('#unfollow-all').attr('disabled', false);
    }
    function stopUnfollowing() {
        stop = true;
        $('#unfollow-all').html('Unfollow all!').unbind('click').click(unfollowAll);
        $('#follow-them-all').attr('disabled', false);
    }
    function trackButton(button) {
    	chrome.runtime.sendMessage({action: 'clickButton', button: button});
    };

    // Main div
    var buttonsTop = theOptions.buttonsTop ? parseInt(theOptions.buttonsTop) : 50;
    $('body').append('<div id="twitter-follower-buttons" style="position: fixed; top: ' + buttonsTop + 'px; z-index: 1000; border: 1px solid #666; border-radius: 3px; background: #FFF; padding: 10px 15px; cursor: move;"></div>');
    $('#twitter-follower-buttons').draggable({containment: "window", stop: function() {
    	console.log($(this).position(), $(this).css('top'), $(this).css('left'));
    	chrome.runtime.sendMessage({action: 'dragButtons', position: $(this).position()});
      }});
    // Add buttons
    $('#twitter-follower-buttons').append('<button id="follow-them-all" class="follow-button btn" type="button"></button>');
    $('#twitter-follower-buttons').append('<button id="unfollow-all" class="follow-button btn" type="button"></button>');
    $('.follow-button').css('display', 'block').css('margin', '6px auto 15px auto');
    $('#twitter-follower-buttons').append('<div style="text-align: center; font-size: 0.85em">If you like this extension,<br>try <a href="https://chrome.google.com/webstore/detail/facebook-follower/egbakddhodbpcephgafohfjoiekelcid" target="_blank">Facebook Follower</a></div>');
    stopFollowing();
    stopUnfollowing();
    // Place on last position (by default, on the right side)
    var buttonsLeft = theOptions.buttonsLeft ? theOptions.buttonsLeft : ($(window).width() - $('#twitter-follower-buttons').outerWidth() - 3);
   	var minWidth = $('#twitter-follower-buttons').width() + 1;
  	$('#twitter-follower-buttons').css('left', buttonsLeft + 'px').css('min-width', minWidth + 'px');
});
